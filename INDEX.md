# The FreeDOS Kernel

The FreeDOS Kernel (supports FAT12/FAT16/FAT32). Includes COUNTRY.SYS, SETVER.SYS and SYS.COM.

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## KERNEL.LSM

<table>
<tr><td>title</td><td>The FreeDOS Kernel</td></tr>
<tr><td>version</td><td>2043</td></tr>
<tr><td>entered&nbsp;date</td><td>2021-05-14</td></tr>
<tr><td>description</td><td>The FreeDOS Kernel</td></tr>
<tr><td>summary</td><td>The FreeDOS Kernel (supports FAT12/FAT16/FAT32). Includes COUNTRY.SYS, SETVER.SYS and SYS.COM.</td></tr>
<tr><td>keywords</td><td>kernel, FreeDOS, DOS, MSDOS</td></tr>
<tr><td>author</td><td>(developers: can be reached on the freedos-kernel mailing list)</td></tr>
<tr><td>maintained&nbsp;by</td><td>freedos-devel@lists.sourceforge.net</td></tr>
<tr><td>primary&nbsp;site</td><td>https://github.com/FDOS/kernel</td></tr>
<tr><td>alternate&nbsp;site</td><td>http://www.fdos.org/kernel/</td></tr>
<tr><td>alternate&nbsp;site</td><td>https://svn.code.sf.net/p/freedos/svn/</td></tr>
<tr><td>platforms</td><td>DOS, FreeDOS, DOSEMU (OpenWatcom C or Turbo C, NASM, UPX)</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU General Public License, Version 2](LICENSE)</td></tr>
</table>
